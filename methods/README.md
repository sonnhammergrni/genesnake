## Methods

Folder for storing gene regulatory network inference methods in.

They do not need to be in python but output should be a csv or json file.
Add details on how genesnake loads data for benchmarking once that is done. 