"""
Init file for adding all benchmarking functions in geneSNAKE to the enviroment. 
Any added function should be added as a defined function and imported here. 
Any added class should be imported in to the genesnake/__init__.py file instead. 
"""

from .compare_networks import compare_networks
from .data_summary import data_summary

