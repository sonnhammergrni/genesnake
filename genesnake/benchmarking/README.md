## GeneSNAKE analysis modules

This directory contains a number of different modules that allows for data, network or predicted networks to be analysed in a number of different ways. Due to the large difference between modules here it is recomended to look at the docstring of each method to understand what they do. 