# GeneSNAKE
A python based tool for generating gene expression data, simulate gene regulatory networks, executing GRNi methods and analysing all of the above.

See official page for more: https://sonnhammer-tutorials.bitbucket.io/genesnake.html